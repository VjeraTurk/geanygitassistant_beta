Geany Git Assistant (BETA)

Init, Commit, Push and Pull directly from your Geany editor.

Requirements
- git
- geany-plugins geanyLua

Place this script in:  /usr/share/geany-plugins/geanylua/edit

Run from geany IDE by choosing 

Tools->LuaScripts->Edit->Commit

This script has it's limitations but works fine in case everything is up to date and origin set correctly.
After all, while developed, this script commited and pushed itself and fellow scripts to remote repository. 
What an inseption, right?!

Feel free to customize it to your needs, change git commands etc.


(Tested on public repositories on github and gitlub, master branches)

geany lua script git geany-lua-script
